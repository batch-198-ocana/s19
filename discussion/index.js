console.log('Hello world!');

//conditional statement
let num1 = 0;

// if statement: executes when condition is met
if(num1 === 0){
    console.log("The value of num1 is 0");
}

// else statement: executes when the previous conditions are not met
let city = "New York";

if(city === "New Jersey"){
    console.log("Welcome to New Jersey!");
}else{
    console.log("This is not New Jersey!");
}

//condition statement in a function
function cityChecker(city){
    if(city === "New York"){
        console.log("Welcome to the Empire State!");
    } else {
        console.log("You're not in New York!");
    }
}
cityChecker("New York");
cityChecker("Los Angeles");

//mini activity
function budgetChecker(num1){
    if(num1 <= 40000){
        console.log("You're still within budget.");
    } else {
        console.log("You are currently over budget.");
    }
}
budgetChecker(20000);
budgetChecker(50000);
//end of mini activity

// else if statement allows execution of task if first condition is not met and the next condition is met
let city2 = "Manila";

if(city2 === "New York"){
    console.log("Welcome to New York!");
} else if(city2 === "Manila"){
    console.log("Welcome to Manila!");
} else {
    console.log("You're lost!");
}

// multiple else if
function determineTyphoonIntensity(windSpeed){
    if(windSpeed < 30){
        return 'Not a typhoon yet';
    } else if(windSpeed <= 61){
        return 'Tropical Depression Detected';
    } else if(windSpeed >= 62 && windSpeed <= 88){
        return 'Tropical Storm Detected';
    } else if(windSpeed >= 89 && windSpeed <= 117){
        return 'Severe tropical Storm Detected';
    } else{
        return 'Typhoon Detected';
    }
}
console.log(determineTyphoonIntensity(29));
console.log(determineTyphoonIntensity(62));
console.log(determineTyphoonIntensity(61));
console.log(determineTyphoonIntensity(88));
console.log(determineTyphoonIntensity(117));
console.log(determineTyphoonIntensity(120));

// truthy and falsy value
if(1){
    console.log("1 is truthy");
}

if([]){
    console.log("[] empty array is truthy");
}

if(0){
    console.log("0 is falsy");
}

if(undefined){
    console.log("Undefined is not falsy");
}else{
    console.log("Undefined is falsy");
}

//conditional ternary operator (alternative to if else statement)
//syntax: (condition) ? ifTrue : ifFalse;
let age = 17;
let result = age < 18 ? "Underage" : "Legal Age";
console.log(result);

//switch statement
let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day){
    case 'monday':
        console.log("The color of the day is red.");
        break;
    case 'tuesday':
        console.log("The color of the day is orange.");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow.");
        break;
    case 'tuesday':
        console.log("The color of the day is green.");
        break;
    case 'friday':
        console.log("The color of the day is gray.");
        break;
    case 'saturday':
        console.log("The color of the day is blue.");
        break;
    case 'sunday':
        console.log("The color of the day is pink.");
        break;
    default:
        console.log("Please enter a valid day");
        break;
}

//try catch
try{
    alerts(determineTyphoonIntensity(50));
} catch(err){
    console.log(typeof err);
    console.log(err);
} finally{
    // finally will run regardless of the success or failure of the try-catch statement
    alert("Intensity updates will show in a new alert");
}