
let username;
let password;
let role;

function login(){
    username = prompt("Enter your username:");
    password = prompt("Enter your password:");
    role = prompt("Enter your role:").toLowerCase();

    if(username == "" || password == "" || role == ""){
        alert("Input must not be empty.");
    } else{
        switch(role){
            case 'admin':
                alert("Welcome back to the class portal, admin!");
                break;
            case 'teacher':
                alert("Thank you for logging in, teacher!");
                break;
            case 'rookie':
                alert("Welcome to the class portal, student!");
                break;
            default:
                alert("Role out of range.");
                break;
        }
    }
}

login();

function checkAverage(num1, num2, num3, num4){
    let average = (num1 + num2 +num3 +num4)/4;
    
    if(Math.round(average) <= 74){
        console.log("Hello, student. your average is " +average +". The letter equivalent is F");
    } else if(Math.round(average) >= 75 && Math.round(average) <= 79){
        console.log("Hello, student. your average is " +average +". The letter equivalent is D");
    } else if(Math.round(average) >= 80 && Math.round(average) <= 84){
        console.log("Hello, student. your average is " +average +". The letter equivalent is C");
    } else if(Math.round(average) >= 85 && Math.round(average) <= 89){
        console.log("Hello, student. your average is " +average +". The letter equivalent is B");
    } else if(Math.round(average) >= 90 && Math.round(average) <= 95){
        console.log("Hello, student. your average is " +average +". The letter equivalent is A");
    } else if(Math.round(average) >= 96){
        console.log("Hello, student. your average is " +average +". The letter equivalent is A+");
    }
}

checkAverage(70,70,72,71);
checkAverage(76,76,77,79);
checkAverage(81,83,84,85);
checkAverage(87,88,88,89);
checkAverage(91,90,92,90);
checkAverage(96,95,97,97);

